# mickey-app-submission

Michelangelo Gerasimou's submission for Reckon Developer Test.

## Project setup
```
npm install
```

### Run program locally
```
npm run serve
```
Open browser at localhost8080.
Click 'Start' to run program.
Click 'Clear' to reset program, to be able to run again.
Important outputs have been console logged.

### Important functionality files
```
- src - components - Application.vue (front end component that displays results/user interacts with/calls main functionality)
      - mixins-and-methods - functionalityMixin.js (main functionality here: API / Calculation related code)
```
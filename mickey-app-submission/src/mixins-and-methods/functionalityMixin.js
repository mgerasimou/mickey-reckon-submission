import Vue from 'vue';

var rp = require('request-promise');

Vue.mixin({
    methods: {
        testFunction: function() {
            console.log('success!');
        },
        hitApi: function(url) {
            var options = {
                uri: url,
                json: true
            };
            return new Promise(function(resolve, reject) {
                rp(options)
                .then(function (repos) {
                    resolve(repos);
                })
                .catch(function (err) {
                    console.log('API call failed...');
                    reject(err);
                });
            })
        },    
        comparisonFunctionEntry: function(api1, api2) {
            var numberRange = range(api1.lower, api1.upper);
            var outputArray = []; // [1:,2:,3: Boss... etc]
            numberRange.forEach(function(rangeValue) {
                console.log(createOutputString(rangeValue, api2));
                outputArray.push(createOutputString(rangeValue, api2));
            })
            // Comparison Program end is here 
            return outputArray;
        } 
    }
})

function createOutputString(rangeValue, api2) {
    // rangeValue = single element of the range array
    var inputString = rangeValue.toString()+': ';
    return inputString.concat(testValue(rangeValue, api2));              
}

function testValue(rangeValue, api2) {
    var combinedString = '';
    api2.outputDetails.forEach(function(divisorValue) {
    if(modulo(rangeValue, divisorValue.divisor) == true) {
        combinedString = combinedString.concat(divisorValue.output);
    }    
});
    return combinedString;
}

function range(start, stop) {
    var a = [start], b = start;
    while (b < stop) {
        a.push(b += 1);
    }
    return a;
}

function modulo(number, divisor) {
    if(number % divisor == 0 && number > 0) {
    // need '&& number > 0' to sanitise zeros and numbers less
        return true;
    }        
}  